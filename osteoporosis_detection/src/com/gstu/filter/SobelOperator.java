package com.gstu.filter;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

import static java.awt.image.BufferedImage.TYPE_INT_RGB;

public class SobelOperator {

    private static int[][] maskY = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };
    private static int[][] maskX = { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };

//    private static int[][] maskY = { { 3, 10, 3 }, { 0, 0, 0 }, { -3, -10, -3 } };
//    private static int[][] maskX = { { 3, 0, -3 }, { 10, 0, -10 }, { 3, 0, -3 } };

    private int[][]pixelMatrix = new int[3][3];

    public BufferedImage sobelOperatorForImg(BufferedImage inputImage) throws IOException {
        int imgWight = inputImage.getWidth();
        int imgHeight = inputImage.getHeight();
        int imgSize = imgWight * imgHeight;

        BufferedImage outputImage = new BufferedImage(imgWight, imgHeight, TYPE_INT_RGB);

        for (int i = 1; i < imgWight - 1; i++) {
            for (int j = 1; j < imgHeight - 1; j++) {
                pixelMatrixNewColor(inputImage, pixelMatrix, i, j);
                int edge =(int) convolution(pixelMatrix);
                outputImage.setRGB(i,j,(edge<<16 | edge<<8 | edge));
            }
        }
        return outputImage;
    }

    private static int[][] pixelMatrixNewColor(BufferedImage inputImage, int[][] pixelMatrix, int imgI, int imgJ) {
        int iCount = imgI - 1;
        for (int i = 0; i < pixelMatrix.length; i++) {
            int jCount = imgJ - 1;
            for (int j = 0; j < pixelMatrix.length; j++) {
                pixelMatrix[i][j] = new Color(inputImage.getRGB(iCount, jCount)).getRed();
                jCount++;
            }
            iCount++;
        }
        return pixelMatrix;
    }

    private static double convolution(int[][] pixelMatrix) {
        int GX = 0, GY = 0;
        for (int i = 0; i < pixelMatrix.length; i++)
            for (int j = 0; j < pixelMatrix.length; j++) {
                GX += pixelMatrix[i][j] * maskX[i][j];
                GY += pixelMatrix[i][j] * maskY[i][j];
            }
        return Math.sqrt(Math.pow(GY, 2) + Math.pow(GX, 2));
    }
}
