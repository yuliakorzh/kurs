package com.gstu.filter;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgcodecs.Imgcodecs;

public class GaussianFilter {

    private static int[][] correctMatrix = {{-1, -1, -1}, {-1, 9, -1}, {-1, -1, -1}};

    public void gaus(String imgPath) {

        try
        {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

            Mat source = Imgcodecs.imread(imgPath,
                    Imgcodecs.CV_LOAD_IMAGE_COLOR);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());
            Imgproc.GaussianBlur(source, destination, new Size(45, 45), 1);

            Imgcodecs.imwrite("src/com/gstu/resources/lumbar_spine/Gaussian45.jpg", destination);

        } catch (Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }
}

