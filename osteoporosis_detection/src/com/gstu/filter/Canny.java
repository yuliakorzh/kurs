package com.gstu.filter;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;


public class Canny {

    int lowThreshold = 50;
    //static final int max_lowThreshold = 100;
    double ratio = 3;
    int kernel_size = 3;

    Mat src, src_gray;
    Mat dst, detected_edges;

    public void CannyFilter(String imgPath, String resultPath) {
        try
        {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

            Mat source = Imgcodecs.imread(imgPath,
                    Imgcodecs.CV_LOAD_IMAGE_COLOR);

            Mat destination = new Mat(source.rows(), source.cols(), source.type());
            Imgproc.Canny(source, destination, lowThreshold, lowThreshold * ratio, kernel_size, false);
            Imgcodecs.imwrite(resultPath, destination);

        } catch (Exception e)
        {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
