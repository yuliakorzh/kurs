//package com.gstu.filter;
//
//import com.pixelmed.dicom.AttributeList;
//import com.pixelmed.dicom.DicomException;
//import com.pixelmed.dicom.ImageToDicom;
//import com.pixelmed.display.SourceImage;
//import org.opencv.core.Core;
//import org.opencv.core.Mat;
//import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.imgproc.Imgproc;
//
//import java.io.IOException;
//
//public class ThresholdFiltering {
//
//    String jpgFile = "dicom/SE1/xr_tspine.jpg";
//    String dicomImgPath2 = "dicom/SE1/xr_tspine.dcm";
//
//    public SourceImage convertToDicom(String jpgFileName, String dicomFileName) throws DicomException {
//        AttributeList list = new AttributeList();;
//        try {
//            //generate the DICOM file from the jpeg file and the other attributes supplied
//            new ImageToDicom(jpgFileName, //path to existing JPEG image
//                    dicomFileName, //output DICOM file with full path
//                    "Saravanan Subramanian", //name of patient
//                    "12121221", //patient id
//                    "2323232322", //study id
//                    "3232323232", //series number
//                    "42423232234"); //instance number
//            //now, dump the contents of the DICOM file to the console
//            list.read(dicomFileName);
//            System.out.println(list.toString());
//        } catch (DicomException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return new SourceImage(list);
//    }
//
//    public SourceImage Sobel(String path) throws DicomException {
//        //Mat m = new Mat(5, 10, CvType.CV_8UC1, new Scalar(0));
//        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
//        Mat gray = Imgcodecs.imread(path, 0);
//        Imgproc.Sobel(gray, gray, gray.depth(), 2, 2);
//        Imgcodecs.imwrite("dicom/SE1/IM1_sobel.jpg", gray);
//        return convertToDicom(jpgFile, dicomImgPath2);
//    }
//
//}
