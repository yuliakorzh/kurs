package com.gstu.dicom;

import com.pixelmed.dicom.*;
import com.pixelmed.display.SourceImage;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class DicomImage {

    static String pathToDicom;

    static AttributeList attributeList;

    public DicomImage (String path) throws IOException, DicomException {
        this.pathToDicom = path;
        attributeList = new AttributeList();
        attributeList.read(path);
    }

    public String getPathToDicom() {
        return pathToDicom;
    }

    public SourceImage sourceImage() throws DicomException {
        return new SourceImage(attributeList);
    }

    public BufferedImage bufferedImage() throws DicomException {
        return sourceImage().getBufferedImage();
    }

    public String getTagInformation(AttributeTag attributeTag) {
        return Attribute.getDelimitedStringValuesOrEmptyString(attributeList, attributeTag);
    }

    public String allTagInformation() {
        String allDicomTags = new String();
        try {
            attributeList.read(pathToDicom);
            allDicomTags += "<html>Patient id: " + getTagInformation(TagFromName.PatientID) + "<br>";
            allDicomTags += "Patient name: " + getTagInformation(TagFromName.PatientName) + "<br>";
            allDicomTags += "Patient age: " + getTagInformation(TagFromName.PatientAge) + "<br>";
            allDicomTags += "Samples Per Pixel: " + getTagInformation(TagFromName.SourceSerialNumber) + "<br>";
            allDicomTags += "Photometric Interpretation: " + getTagInformation(TagFromName.PhotometricInterpretation) + "<br>";
            allDicomTags += "Pixel Spacing: " + getTagInformation(TagFromName.PixelSpacing) + "<br>";
            allDicomTags += "Bits Allocated: " + getTagInformation(TagFromName.BitsAllocated) + "<br>";
            allDicomTags += "Bits Stored: " + getTagInformation(TagFromName.BitsStored) + "<br>";
            allDicomTags += "High Bit: " + getTagInformation(TagFromName.HighBit) + "</html>";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return allDicomTags;
    }
}
