package com.gstu.dicom;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;


import com.gstu.filter.SobelOperator;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.ImageToDicom;
import com.pixelmed.display.SingleImagePanel;
import com.pixelmed.display.SourceImage;

public class ViewDicomImage {

    public static void main(String[] args) throws IOException, DicomException {
//        String dicomImgPath = "dicom/SE1/MR";
//        String jpgFileDicom = "dicom/SE1/xr_tspine.jpg";
//        String jpgFile = "dicom/SE1/IM1_sobel.jpg";
//        String dicomImgPath2 = "dicom/SE1/IM1";
//        DicomImage dicomImage = new DicomImage(dicomImgPath);
//        SourceImage sourceImage = dicomImage.sourceImage();
//        System.out.println("Number of frames: " + sourceImage.getNumberOfFrames());

        String dicomImagePath = "src/com/gstu/resources/lumbar_spine/rentgen.dcm";
        DicomImage dicomImage = new DicomImage(dicomImagePath);
        SourceImage sourceImage = dicomImage.sourceImage();
        SingleImagePanel singleImagePanel = new SingleImagePanel(sourceImage);

        SingleImagePanel singleImagePanel2 = new SingleImagePanel(sourceImage);

        JFrame frame = new JFrame();


        frame.getContentPane().add(singleImagePanel);
        //frame.add(panel2);
        frame.setSize(1300,700);
        frame.setTitle("Vertebrae recognition");

        frame.setVisible(true);


//
//        new GaussianFilter().gaus("dicom/SE1/rentgen3.jpg");
//        File f = new File("dicom/SE1/Gaussian45.jpg");
//        BufferedImage image = ImageIO.read(f);
//        BufferedImage outputImage = new SobelOperator().sobelOperatorForImg(image);
//
//        File outputfile = new File("dicom/SE1/MINE_SOBEL.jpg");
//        ImageIO.write(outputImage,"jpg", outputfile);
////        new SobelOperator().sobelOperatorForImg(outputfile.);
//
//        new ImageToDicom("dicom/SE1/rentgen3.jpg", //path to existing JPEG image
//                "dicom/SE1/MINE_SOBEL_NEW_DICOM.dcm", //output DICOM file with full path
//                "Saravanan Subramanian", //name of patient
//                "12121221", //patient id
//                "2323232322", //study id
//                "3232323232", //series number
//                "42423232234"); //ins

//        JFrame frame2 = new JFrame();
//        ConsumerFormatImageMaker.convertFileToEightBitImage(dicomImgPath, jpgFileDicom, "jpeg", 0);
//        OverriddenSingleImagePanel singleImagePane2 = new OverriddenSingleImagePanel(new ThresholdFiltering().Sobel(jpgFileDicom));
//        frame2.add(singleImagePane2);
//        frame2.setBackground(Color.BLACK);
//        frame2.setSize(sourceImage.getWidth(), sourceImage.getHeight());
//        //frame.setTitle("Demo for view, scroll and window width/level operations");
//        frame2.setVisible(true);


//        String outputJpgFile = "dicom/SE1/IM1.jpg";
//        ConsumerFormatImageMaker.convertFileToEightBitImage(dicomImgPath, outputJpgFile, "jpeg", 0);
//        new ThresholdFiltering().Sobel(outputJpgFile);
//        JFrame frame2 = new JFrame();
//        //OverriddenSingleImagePanel singleImagePane2= new OverriddenSingleImagePanel(sourceImage2);
//
//        ImagePanel imagePanel = new ImagePanel(dicomImgPath2);
//        frame2.add(imagePanel);
//        frame.setBackground(Color.BLACK);
//        frame.setSize(sourceImage.getWidth(), sourceImage.getHeight());
//        //frame.setTitle("Demo for view, scroll and window width/level operations");
//        frame.setVisible(true);


        String dicomImgPath = "src/com/gstu/resources/lumbar_spine/lumbar.dcm";

        String jpgResult = "src/com/gstu/resources/lumbar_spine/MINE_SOBEL.jpg";

//        DicomImage dicomImage = new DicomImage(dicomImgPath);
//        SourceImage sourceImage = dicomImage.sourceImage();

//        new GaussianFilter().gaus("src/com/gstu/resources/lumbar_spine/rentgen.jpg");
        File f = new File("src/com/gstu/resources/lumbar_spine/Gaussian45.jpg");
//        File f = new File("src/com/gstu/resources/lumbar_spine/rentgen.jpg");
        BufferedImage image = ImageIO.read(f);
        BufferedImage outputImage = new SobelOperator().sobelOperatorForImg(image);


        File outputfile = new File(jpgResult);
        ImageIO.write(outputImage,"jpg", outputfile);

        String result = "src/com/gstu/resources/lumbar_spine/CANNY.jpg";

//        new Canny().CannyFilter(jpgResult, result);
    }

}
