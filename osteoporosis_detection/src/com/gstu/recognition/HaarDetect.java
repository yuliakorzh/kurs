package com.gstu.recognition;

import org.opencv.core.*;

import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class HaarDetect {

    public static Mat bufferedImageToMat(BufferedImage bi) {
        Mat mat = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC3);
        byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        return mat;
    }

    public static void detect(Mat src) {

        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        CascadeClassifier cascadeFaceClassifier = new CascadeClassifier(
                "src/com/gstu/resources/haarcascade_vertebrae_spine.xml");

        MatOfRect pozvonki = new MatOfRect();
        cascadeFaceClassifier.detectMultiScale(src, pozvonki);
        //Yakalanan çerçeve varsa içerisinde dön ve yüzün boyutlary ölçüsünde bir kare çiz
        for (Rect rect : pozvonki.toArray()) {
            //Sol üst kö?esine metin yaz
            //Imgproc.putText(src, "Face", new Point(rect.x,rect.y-5), 1, 2, new Scalar(0,0,255));
            Imgproc.rectangle(src, new Point(rect.x, rect.y), new Point(rect.x + rect.width, rect.y + rect.height),
                    new Scalar(0, 100, 0),3);
        }

    }
}
