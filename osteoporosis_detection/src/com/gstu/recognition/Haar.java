package com.gstu.recognition;

import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;

public class Haar {

    public static final String XML_FILE =
            "src/com/gstu/resources/haarcascade_vertebrae_spine.xml";

    public static void detect(IplImage sourceDicomImage){

        CvHaarClassifierCascade cascade = new CvHaarClassifierCascade(cvLoad(XML_FILE));
        CvMemStorage storage = CvMemStorage.create();
        CvSeq spine = cvHaarDetectObjects(
                sourceDicomImage,
                cascade,
                storage,
                1.5,
                3,
                CV_HAAR_DO_CANNY_PRUNING);

        cvClearMemStorage(storage);

        int totalVertebrae = spine.total();

        for(int i = 0; i < totalVertebrae; i++){
            CvRect r = new CvRect(cvGetSeqElem(spine, i));
            cvRectangle (
                    sourceDicomImage,
                    cvPoint(r.x(), r.y()),
                    cvPoint(r.width() + r.x(), r.height() + r.y()),
                    CvScalar.RED,
                    2,
                    CV_AA,
                    0);

        }

        cvShowImage("Detected spine", sourceDicomImage);
        cvWaitKey(0);
    }
}
